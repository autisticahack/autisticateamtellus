package com.tellus.autisticaapp.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Message {
	
	public String userName;
	public String date;
	public String message;

	public Message(String jsonString) {
		JSONObject jsonObj = null;
		try {
			jsonObj = new JSONObject(jsonString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(jsonObj != null) {
			userName = jsonObj.optString("user");
			date = jsonObj.optString("date");
			message = jsonObj.optString("message");
		}
	}

}
