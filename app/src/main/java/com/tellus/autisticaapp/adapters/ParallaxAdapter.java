package com.tellus.autisticaapp.adapters;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.poliveira.parallaxrecyclerview.ParallaxRecyclerAdapter;
import com.tellus.autisticaapp.R;
import com.tellus.autisticaapp.models.Person;

import java.util.List;


public class ParallaxAdapter extends ParallaxRecyclerAdapter<Person> {
    List<Person> persons;
    private final Context mContext;
    public ParallaxAdapter(List<Person> persons, Context baseContext) {
        super(persons);
        this.persons = persons;
        this.mContext = baseContext;
    }

    static class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        CardView card;
        TextView personName;
        TextView info;
        ImageView personPhoto;
        public IMyViewHolderClicks mListener;

        public PersonViewHolder(View itemView, IMyViewHolderClicks listener) {
            super(itemView);

            mListener = listener;

            card = (CardView) itemView.findViewById(R.id.card);
            personName = (TextView) itemView.findViewById(R.id.person_name);
            info = (TextView) itemView.findViewById(R.id.info);
            personPhoto = (ImageView) itemView.findViewById(R.id.person_photo);

            personPhoto.setOnClickListener(this);
            card.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view instanceof ImageView) {
                mListener.tapImage((ImageView) view, itemView);
            } else {
                mListener.tapCard(view);
            }
        }

        public interface IMyViewHolderClicks {


            void tapImage(ImageView callerImage, View itemView);

            void tapCard(View caller);
        }
    }

    @Override
    public void onBindViewHolderImpl(RecyclerView.ViewHolder viewHolder, ParallaxRecyclerAdapter<Person> adapter, int i) {
        ((PersonViewHolder) viewHolder).personName.setText(persons.get(i).name);
        ((PersonViewHolder) viewHolder).info.setText(persons.get(i).info);
        ((PersonViewHolder) viewHolder).personPhoto.setImageResource(persons.get(i).photoId);

        ((PersonViewHolder) viewHolder).personName.setTag(i);
        ((PersonViewHolder) viewHolder).info.setTag(i);
        ((PersonViewHolder) viewHolder).personPhoto.setTag(i);
        ((PersonViewHolder) viewHolder).card.setTag(i);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup viewGroup, ParallaxRecyclerAdapter<Person> parallaxRecyclerAdapter, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycle_card_view, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v, new PersonViewHolder.IMyViewHolderClicks() {


            @Override
            public void tapImage(ImageView callerImage, View itemView) {
                int viewTag = (int) callerImage.getTag();
               animate(callerImage);
            }

            @Override
            public void tapCard(View caller) {
                int viewTag = (int) caller.getTag();
                animate(caller);
            }
        });
        return pvh;
    }

    private void animate(View view) {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view,
                "scaleX", 0.9f);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(view,
                "scaleY", 0.9f);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(view,
                "scaleX", 1f);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(view,
                "scaleY", 1f);
        AnimatorSet animSet = new AnimatorSet();
        anim1.setInterpolator(new DecelerateInterpolator());
        anim2.setInterpolator(new DecelerateInterpolator());
        anim3.setInterpolator(new AccelerateDecelerateInterpolator());
        anim4.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4).after(80);
        animSet.setDuration(80);
        animSet.start();
    }

    @Override
    public int getItemCountImpl(ParallaxRecyclerAdapter<Person> parallaxRecyclerAdapter) {
        return persons.size();
    }
}