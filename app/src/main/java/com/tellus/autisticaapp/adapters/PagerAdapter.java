package com.tellus.autisticaapp.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.tellus.autisticaapp.R;
import com.tellus.autisticaapp.fragments.Fragment1;
import com.tellus.autisticaapp.fragments.Fragment2;
import com.tellus.autisticaapp.fragments.ScrollFragmentWebView;

public class PagerAdapter extends FragmentPagerAdapter {

    String tabTitles[] = new String[]{"Home", "Explain", "Manage"};
    Context context;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ScrollFragmentWebView();
            case 1:
                return new Fragment1();
            case 2:
                return new Fragment2();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public View getTabView(int position) {
        View tab = LayoutInflater.from(context).inflate(R.layout.pager_custom_tab, null);
        TextView tv = (TextView) tab.findViewById(R.id.custom_text);
        tv.setText(tabTitles[position]);
        return tab;
    }
}
