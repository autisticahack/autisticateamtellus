package com.tellus.autisticaapp.adapters;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tellus.autisticaapp.R;
import com.tellus.autisticaapp.models.Message;
import com.tellus.autisticaapp.utils.TinyDB;

import java.util.ArrayList;

public class ChatListAdapter extends ArrayAdapter<Message> {
    private TextView messageTextView;
    private TextView timeTextView;
    private RelativeLayout chatBubble;
    private TextView authorTextView;
    private RelativeLayout chatGravity;

    private final Context mContext;
    private final TinyDB tinydb;
    private LayoutInflater mInflater;

    public ChatListAdapter(Context context, ArrayList<Message> items) {
        super(context, 0, items);
        mContext = context;
        this.mInflater = LayoutInflater.from(context);
        tinydb = new TinyDB(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message message = getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_chat_message, null, false);
        }

        messageTextView = (TextView) convertView.findViewById(R.id.textViewMessage);
        timeTextView = (TextView) convertView.findViewById(R.id.textViewTime);
        chatBubble = (RelativeLayout) convertView.findViewById(R.id.chat_bubble);
        authorTextView = (TextView) convertView.findViewById(R.id.textViewName);
        chatGravity = (RelativeLayout) convertView.findViewById(R.id.chat_gravity_layout);

        timeTextView.setText(message.date);
        authorTextView.setText(message.userName);
        messageTextView.setText(message.message);
        chatBubble = (RelativeLayout) convertView.findViewById(R.id.chat_bubble);
        chatGravity = (RelativeLayout) convertView.findViewById(R.id.chat_gravity_layout);



        if (message.userName.equals(tinydb.getString("myUserName"))) {
            chatBubble.setBackgroundResource(R.drawable.balloon_outgoing_normal);
            timeTextView.setPadding(0, 0, pxFromDp(20), pxFromDp(5));
            chatGravity.setGravity(GravityCompat.END);
            authorTextView.setVisibility(View.GONE);
        } else {
            chatBubble.setBackgroundResource(R.drawable.balloon_incoming_normal);
            timeTextView.setPadding(0, 0, pxFromDp(10), pxFromDp(5));
            chatGravity.setGravity(GravityCompat.START);
            authorTextView.setVisibility(View.VISIBLE);
            authorTextView.setText(message.userName);
        }

        return convertView;
    }

    public int pxFromDp(final float dp) {
        return (int) (dp * mContext.getResources().getDisplayMetrics().density);
    }

}
