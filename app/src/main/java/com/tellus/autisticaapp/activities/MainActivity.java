package com.tellus.autisticaapp.activities;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.tellus.autisticaapp.R;
import com.tellus.autisticaapp.adapters.ChatListAdapter;
import com.tellus.autisticaapp.adapters.PagerAdapter;
import com.tellus.autisticaapp.models.Message;
import com.tellus.autisticaapp.utils.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.codetail.animation.ViewAnimationUtils;

public class MainActivity extends AppCompatActivity {
    private FloatingActionButton fab;
    private android.support.v7.widget.Toolbar toolbar;
    private LinearLayout revealLinearLayout;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    ImageView slidingPanelArrow;
    private AppBarLayout bar;
    private TabLayout tabLayout;
    private FloatingActionButton sendMessage;
    private LinearLayout slidingContent;
    private TinyDB tinydb;
    private Pubnub pubnub;
    private ArrayList<Message> convoArrayList = new ArrayList<Message>();
    private ChatListAdapter arrayAdapter;
    private EditText messageEditText;
    private ListView lv;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pubnub = new Pubnub("",
                "", false);
        history("chats");
        setupSub();
        setupIds();
        initialiseViews();
    }

    private void initialiseViews() {
        if (tinydb.getString("myUserName") == null || tinydb.getString("myUserName").length() < 1) {
            setUserName();
        }
        revealLinearLayout.setVisibility(View.INVISIBLE);
        toolbar.getBackground().setAlpha(140);
        toolbar.setTitleTextAppearance(this, R.style.MyTitleTextApperance);
        lv.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2); //on page 3 dont destroy page 1
        setupSlidingPanelLayout();
        setSupportActionBar(toolbar);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateFAB(fab);
                animateCircleOutwardsFromView(fab);

            }
        });
        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animateFAB(sendMessage);
                pub();
            }
        });
    }

    private void setupIds() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        revealLinearLayout = (LinearLayout) findViewById(R.id.revealLinearLayout);
        slidingContent = (LinearLayout) findViewById(R.id.slidingContent);
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        slidingPanelArrow = (ImageView) findViewById(R.id.sliding_panel_arrow);
        bar = (AppBarLayout) findViewById(R.id.appBarLayout);
        lv = (ListView) findViewById(R.id.convo);
        arrayAdapter = new ChatListAdapter(this.getBaseContext(), convoArrayList);
        messageEditText = (EditText) findViewById(R.id.messageInputBox);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout.setupWithViewPager(viewPager);
        sendMessage = (FloatingActionButton) findViewById(R.id.sendMessage);
        fab = (FloatingActionButton) findViewById(R.id.fabButton);
        tinydb = new TinyDB(getBaseContext());
        pagerAdapter =
                new PagerAdapter(getSupportFragmentManager(), MainActivity.this);
    }


    private void setupSlidingPanelLayout() {
        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                slidingContent.setAlpha(slideOffset); //((float) (0.5 + (slideOffset/2))
                toolbar.setAlpha(1 - slideOffset);
                bar.setAlpha(1 - slideOffset);
                fab.setAlpha(1 - slideOffset);
                if (slideOffset == 1) {
                    toolbar.setVisibility(View.GONE);
                    bar.setVisibility(View.GONE);
                    tabLayout.setVisibility(View.GONE);
                    fab.setVisibility(View.GONE);
                    fab.setClickable(false);
                } else {
                    toolbar.setVisibility(View.VISIBLE);
                    bar.setVisibility(View.VISIBLE);
                    tabLayout.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.VISIBLE);
                    fab.setClickable(true);
                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    slidingPanelArrow.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.ic_keyboard_arrow_down_white_48dp));
                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    slidingPanelArrow.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.ic_keyboard_arrow_up_white_48dp));

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        revealLinearLayout.setVisibility(View.INVISIBLE);
        animateViewFadeIn(bar);
        animateViewFadeIn(fab);
        animateViewFadeIn(slidingContent);
        fab.setEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.chat) {
            if (slidingUpPanelLayout.getPanelState() != SlidingUpPanelLayout.PanelState.EXPANDED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
            if (slidingUpPanelLayout.getPanelState() != SlidingUpPanelLayout.PanelState.COLLAPSED) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
            return true;
        }
        if (id == R.id.rename) {
            setUserName();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void animateFAB(View fab) {
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(fab,
                "scaleX", 0.8f);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(fab,
                "scaleY", 0.8f);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(fab,
                "scaleX", 1f);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(fab,
                "scaleY", 1f);
        AnimatorSet animSet = new AnimatorSet();
        anim1.setInterpolator(new DecelerateInterpolator());
        anim2.setInterpolator(new DecelerateInterpolator());
        anim3.setInterpolator(new AccelerateDecelerateInterpolator());
        anim4.setInterpolator(new AccelerateDecelerateInterpolator());
        animSet.play(anim1).with(anim2);
        animSet.play(anim3).with(anim4).after(80);
        animSet.setDuration(80);
        animSet.start();
    }

    private void animateCircleOutwardsFromView(View view) {
        int[] startingLocation = new int[2];
        view.getLocationOnScreen(startingLocation);
        startingLocation[0] += view.getWidth() / 2;

        revealLinearLayout.setVisibility(View.VISIBLE);

        int cx = (startingLocation[0]);
        int cy = (startingLocation[1]);

        int dx = Math.max(cx, view.getWidth() - cx);
        int dy = Math.max(cy, view.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);
        Animator.AnimatorListener listener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Intent intent = new Intent(getApplicationContext(), BreathActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        };
        Animator animator =
                ViewAnimationUtils.createCircularReveal(revealLinearLayout, cx, cy, 0, finalRadius);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.setDuration(500);
        animator.addListener(listener);
        animator.start();
        AppBarLayout bar = (AppBarLayout) findViewById(R.id.appBarLayout);
        animateViewFadeOut(fab);
        animateViewFadeOut(bar);
        animateViewFadeOut(slidingContent);
    }

    private void animateViewFadeOut(View view) {
        Animation a = new AlphaAnimation(1.0f, 0.0f);
        a.setDuration(500);
        a.setFillAfter(true);
        view.startAnimation(a);
    }

    private void animateViewFadeIn(View view) {
        Animation a = new AlphaAnimation(0.0f, 1.0f);
        a.setDuration(0);
        a.setFillAfter(true);
        view.startAnimation(a);
    }

    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout != null &&
                (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }


    private void setUserName() {
        final AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                .create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        final View layout = inflater.inflate(R.layout.dialog_new_user, null);
        alert.setView(layout);
        alert.setCancelable(false);
        alert.show();
        final ImageView confirmDetails = (ImageView) layout
                .findViewById(R.id.confirmDetails);

        EditText userNameEditText = (EditText) layout
                .findViewById(R.id.userName);


        userNameEditText.setText(tinydb.getString("myUserName"));
        confirmDetails.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                EditText userNameEditText = (EditText) layout
                        .findViewById(R.id.userName);

                String userName = userNameEditText.getText().toString();
                if (userName.length() > 1) {
                    tinydb.putString("myUserName", userName);
                    triggerArrayAdapterDataSetChanged();
                    alert.dismiss();
                } else {

                    Toast.makeText(getApplicationContext(),
                            "Name must be at least two letters long.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    void history(String channel) {
        pubnub.history(channel, 20, new Callback() {
            @Override
            public void successCallback(String channel, Object message) {
                JSONArray jArray;
                jArray = (JSONArray) message;
                receivedNewHistory(jArray);
            }

            @Override
            public void errorCallback(String channel, PubnubError error) {
            }
        });
    }

    public void receivedNewHistory(JSONArray jArray) {
        try {
            JSONArray inner = jArray.getJSONArray(0);
            if (inner != null) {
                for (int i = 0; i < inner.length(); i++) {
                    try {

                        convoArrayList.add(new Message(inner.getString(i)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        triggerArrayAdapterDataSetChanged();
    }

    private void pub() {
        String message = messageEditText.getText().toString();
        messageEditText.setText("");
        JSONObject json = new JSONObject();
        String userName = tinydb.getString("myUserName");
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("HH:mm");
        try {
            json.put("type", "message");
            json.put("message", message);
            json.put("user", userName);
            json.put("date", ft.format(dNow));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Callback callback = new Callback() {
            public void successCallback(String channel, Object response) {
            }

            public void errorCallback(String channel, PubnubError error) {
            }
        };
        if (message.length() > 0) {
            pubnub.publish("chats", json, callback);
        }
    }

    private void setupSub() {
        try {
            pubnub.subscribe(new String[]{"chats"}, new Callback() {
                public void connectCallback(String channel) {
                }

                public void disconnectCallback(String channel) {

                }

                public void reconnectCallback(String channel) {
                }

                @Override
                public void successCallback(String channel, Object message) {
                    JSONObject jsonObj = null;
                    String dataType = null;
                    try {
                        jsonObj = new JSONObject(message.toString());
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    if (jsonObj != null) {
                        dataType = jsonObj.optString("type");
                    }
                    if (dataType.equals("message")) {
                        convoArrayList.add(new Message(message.toString()));
                        triggerArrayAdapterDataSetChanged();
                    } else if (dataType.equals("table")) {
                    }
                }

                @Override
                public void errorCallback(String channel, Object message) {
                }
            });
        } catch (PubnubException e) {
        }
    }

    public void triggerArrayAdapterDataSetChanged() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                arrayAdapter.notifyDataSetChanged();
            }
        });
    }

}
