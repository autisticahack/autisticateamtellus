package com.tellus.autisticaapp.activities;

/**
 * Created by Admin on 09/11/2016.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tellus.autisticaapp.R;

import java.util.Random;


/**
 * Created by Admin on 09/11/2016.
 */

public class BreathActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener {
    private RelativeLayout parentLayout;
    private RelativeLayout colorBackgroundLayout;
    private int previousColor = 1;
    FrameLayout parentView;
    private int backgroundNewColour;
    private int duration = 4000;
    public boolean colorExists = false;
    private ImageButton circleButton;
    TextView inOut;
    private TextView breathe;
    private TextureView mTextureView;
    private Paint mPaint = new Paint();
    private BreathActivity.RenderingThread mThread;
    private float mCenterX;
    private float mCenterY;
    private int previousContrastingColour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        setContentView(R.layout.activity_breath);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);

        animateViewFadeOut(parentLayout);

        Bitmap pixel = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        pixel.eraseColor(-1);
        Drawable drawable = new BitmapDrawable(getResources(), pixel);
        drawable.setAlpha(50);


        colorBackgroundLayout = (RelativeLayout) findViewById(R.id.colorlayout);
        circleButton = (ImageButton) findViewById(R.id.circleButton);
        circleButton.setBackgroundResource(R.drawable.circle);
        parentView = (FrameLayout) findViewById(R.id.frame);
        inOut = (TextView) findViewById(R.id.in_out);
        breathe = (TextView) findViewById(R.id.breathe);

        rainbowBackground();
        circleButton.setEnabled(false);
        circleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setPivotX((float) v.getWidth() / 2);
                v.setPivotY((float) v.getWidth() / 2);

                if (inOut.getText().equals("in")) {
                    inOut.setText("out");
                    viewResize(v, 1400, (float) 1.2);
                } else {
                    inOut.setText("in");
                    viewResize(v, 1400, (float) 1);
                }
                rainbowBackground();
            }
        });

        mTextureView = (TextureView) findViewById(R.id.canvasTextureView);
        mTextureView.setSurfaceTextureListener(this);
        mTextureView.setOpaque(false);

        final Paint paint = mPaint;
        paint.setColor(0xffffffff);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(7);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.STROKE);
    }


    private int randomColor() {
        Random r = new Random();
        int red = r.nextInt(256);
        int green = r.nextInt(256);
        int blue = r.nextInt(256);
        return Color.rgb(red, green, blue);
    }

    private void rainbowBackground() {
        if (!colorExists) {//
            previousColor = randomColor();
        }

        backgroundNewColour = randomColor();
        int red = (backgroundNewColour >> 16) & 0xFF;
        int green = (backgroundNewColour >> 8) & 0xFF;
        int blue = backgroundNewColour & 0xFF;
        int contrastingColor = ContrastColor(red, green, blue);


        changeBackgroundColor(colorBackgroundLayout, previousColor,
                backgroundNewColour, duration);
        if (inOut.isShown()) {
            changeTextColor(breathe, previousContrastingColour, contrastingColor, 1800);
            changeTextColor(inOut, previousContrastingColour, contrastingColor, 1800);
            changePaintColor(mPaint, previousContrastingColour, contrastingColor, 1800);
        }
        previousContrastingColour = contrastingColor;
        duration = 1800; // subsequent times
        previousColor = backgroundNewColour;
        colorExists = true;
    }

    private int ContrastColor(int red, int green, int blue) {
        int d = 0;
        double a = 1 - (0.299 * red + 0.587 * green + 0.114 * blue) / 255;
        if (a < 0.5) {
            d = 0;
        } else {
            d = 255;
        }
        return Color.rgb(d, d, d);
    }

    private void changeBackgroundColor(final View view, int startColor, int endColor,
                                       int duration) {
        ObjectAnimator colorFade = ObjectAnimator.ofObject(view,
                "backgroundColor", new ArgbEvaluator(), (startColor), endColor);
        colorFade.setDuration(duration);
        colorFade.addListener(new AnimatorListenerAdapter() {
            private boolean mIsCanceled = false;
            @Override
            public void onAnimationCancel(Animator animation) {
                mIsCanceled = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!mIsCanceled) {
                    circleButton.callOnClick();
                }
            }
        });
        colorFade.start();
    }

    private void changeTextColor(View view, int startColor, int endColor,
                                 int duration) {
        ObjectAnimator colorFade = ObjectAnimator.ofObject(view, "textColor",
                new ArgbEvaluator(), (startColor), endColor);
        //colorFade.setInterpolator(new FastOutLinearInInterpolator());
        colorFade.setDuration(duration);
        colorFade.start();
    }

    private void changePaintColor(Paint paint, int startColor, int endColor,
                                  int duration) {


        ObjectAnimator colorFade = ObjectAnimator.ofObject(paint, "color", new ArgbEvaluator(), startColor, endColor);
        //colorFade.setInterpolator(new FastOutLinearInInterpolator());
        colorFade.setDuration(duration);
        colorFade.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // TODO Auto-generated method stub
                //invalidate();

            }


        });

        colorFade.start();
    }

    private void viewResize(View view, int duration, float scale) {
        ObjectAnimator scaleDown = ObjectAnimator
                .ofPropertyValuesHolder(view,
                        PropertyValuesHolder.ofFloat("scaleX", scale),
                        PropertyValuesHolder.ofFloat("scaleY", scale));
        scaleDown.setDuration(duration);
        scaleDown.start();
    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mThread = new BreathActivity.RenderingThread(mTextureView);
        mThread.start();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mThread != null) mThread.stopRendering();
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    private volatile boolean mRunning = true;

    private class RenderingThread extends Thread {
        private final TextureView mSurface;


        public RenderingThread(TextureView surface) {
            mSurface = surface;
        }

        @Override
        public void run() {


            while (mRunning && !Thread.interrupted()) {
                final Canvas canvas = mSurface.lockCanvas(null);
                mCenterX = canvas.getWidth() / 2.0f;
                mCenterY = canvas.getHeight() / 2.0f;
                try {
                    canvas.drawColor(0x00000000, PorterDuff.Mode.CLEAR);

                    drawCube(canvas);


                } finally {
                    mSurface.unlockCanvasAndPost(canvas);
                }


                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    // Interrupted
                }
            }
        }

        void stopRendering() {
            interrupt();
            mRunning = false;
        }
    }


    void drawCube(Canvas c) {
        c.save();
        c.translate(mCenterX, mCenterY);
        c.drawColor(Color.TRANSPARENT);

        drawLine(c, -420, -420, -420, 420, -420, -420);
        drawLine(c, 420, -420, -420, 420, 420, -420);
        drawLine(c, 420, 420, -420, -420, 420, -420);
        drawLine(c, -420, 420, -420, -420, -420, -420);

        drawLine(c, -420, -420, 420, 420, -420, 420);
        drawLine(c, 420, -420, 420, 420, 420, 420);
        drawLine(c, 420, 420, 420, -420, 420, 420);
        drawLine(c, -420, 420, 420, -420, -420, 420);

        drawLine(c, -420, -420, 420, -420, -420, -420);
        drawLine(c, 420, -420, 420, 420, -420, -420);
        drawLine(c, 420, 420, 420, 420, 420, -420);
        drawLine(c, -420, 420, 420, -420, 420, -420);
        c.restore();
    }

    void drawLine(Canvas c, int x1, int y1, int z1, int x2, int y2, int z2) {
        long now = SystemClock.elapsedRealtime();

        float xrot = ((float) (now)) / 1800;
        float yrot = ((float) (now)) / 1500;
        float zrot = 0;

        float newy1 = (float) (Math.sin(xrot) * z1 + Math.cos(xrot) * y1);
        float newy2 = (float) (Math.sin(xrot) * z2 + Math.cos(xrot) * y2);
        float newz1 = (float) (Math.cos(xrot) * z1 - Math.sin(xrot) * y1);
        float newz2 = (float) (Math.cos(xrot) * z2 - Math.sin(xrot) * y2);

        float newx1 = (float) (Math.sin(yrot) * newz1 + Math.cos(yrot) * x1);
        float newx2 = (float) (Math.sin(yrot) * newz2 + Math.cos(yrot) * x2);
        newz1 = (float) (Math.cos(yrot) * newz1 - Math.sin(yrot) * x1);
        newz2 = (float) (Math.cos(yrot) * newz2 - Math.sin(yrot) * x2);

        float startX = newx1 / (4 - newz1 / 400);
        float startY = newy1 / (4 - newz1 / 400);
        float stopX = newx2 / (4 - newz2 / 400);
        float stopY = newy2 / (4 - newz2 / 400);

        c.drawLine(startX, startY, stopX, stopY, mPaint);
    }

    private void animateViewFadeOut(View view) {
        Animation a = new AlphaAnimation(1.0f, 0.0f);
        a.setDuration(500);
        a.setFillAfter(true);
        view.startAnimation(a);
    }


    @Override
    protected void onPause() {
        super.onPause();
        mRunning = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRunning = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mThread.stopRendering();
    }
}

