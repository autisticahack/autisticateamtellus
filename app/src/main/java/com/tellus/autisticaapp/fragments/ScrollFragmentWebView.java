package com.tellus.autisticaapp.fragments;

import android.content.res.TypedArray;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.tellus.autisticaapp.R;


public class ScrollFragmentWebView extends Fragment {

    private WebView mWebView;
    private int mActionBarSize;
    private FrameLayout framelayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scroll_webview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        framelayout = (FrameLayout) view.findViewById(R.id.framelayout);
        mWebView = (WebView) view.findViewById(R.id.webView);
        mWebView.setWebViewClient(new SSLTolerentWebViewClient());
        injectHeader(mWebView, true);
        mWebView.loadUrl("https://www.autistica.org.uk/");
        int padding = getResources().getDimensionPixelOffset(R.dimen.tabsHeight);
        final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        framelayout.setPadding(0, padding + mActionBarSize, 0, 0);

    }

    private class SSLTolerentWebViewClient extends WebViewClient {
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            handler.proceed();
        }
    }

    public static void injectHeader(final WebView webView, boolean withAnimation) {
        if (webView != null) {
            WebSettings webSettings = webView.getSettings();
            webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

            webSettings.setJavaScriptEnabled(true);
            webSettings.setDomStorageEnabled(true);

            if (android.os.Build.VERSION.SDK_INT >= 11) {
                //transparent background
                webView.setLayerType(WebView.LAYER_TYPE_NONE, null);
            }

        }
    }


}
