package com.tellus.autisticaapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tellus.autisticaapp.R;
import com.tellus.autisticaapp.adapters.RecycleAdapter;
import com.tellus.autisticaapp.models.Person;

import java.util.ArrayList;
import java.util.List;

;

public class Fragment3 extends Fragment {
    private List<Person> persons;

    public Fragment3() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_3, container, false);
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);

        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        initData();
        RecycleAdapter adapter = new RecycleAdapter(persons);
        rv.setAdapter(adapter);
        return rootView;
    }

    private void initData() {
        persons = new ArrayList<>();
        persons.add(new Person("Dave", "Java guy", R.drawable.i1));
        persons.add(new Person("Muhammad", "Oculus Rift", R.drawable.i4));
        persons.add(new Person("Ed", "Android guy", R.drawable.i1));
        persons.add(new Person("Zain", "And a street cat named Bob", R.drawable.i4));
        persons.add(new Person("Ciara", "Milkshake", R.drawable.i1));
        persons.add(new Person("Bo", "Open seasame", R.drawable.i4));
        persons.add(new Person("Guest 1", "???", R.drawable.i1));
        persons.add(new Person("Guest 2", "???", R.drawable.i4));
    }
}