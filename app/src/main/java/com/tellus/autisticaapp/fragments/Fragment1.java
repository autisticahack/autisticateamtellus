package com.tellus.autisticaapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tellus.autisticaapp.R;
import com.tellus.autisticaapp.adapters.ParallaxAdapter;
import com.tellus.autisticaapp.models.Person;

import java.util.ArrayList;
import java.util.List;

;

public class Fragment1 extends Fragment {
    private List<Person> persons;

    public Fragment1() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_1, container, false);
        RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);

        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        initData();
        ParallaxAdapter adapter = new ParallaxAdapter(persons, getActivity().getBaseContext());
        rv.setAdapter(adapter);

        View header = getActivity().getLayoutInflater().inflate(R.layout.parralax_recycle_header, rv, false);
        adapter.setParallaxHeader(header, rv);

        return rootView;
    }

    private void initData() {
        persons = new ArrayList<>();
        persons.add(new Person("What is ASD", "ASD stands for autism SPECTRUM disorder. ASD affects how you experience the world around you, and can change the way that you behave or communicate. \n", R.drawable.number1));
        persons.add(new Person("Feelings", "You may have heard some people talk about the “fight or flight” response. \n\nThis is one way of describing how our body responds to either real danger or just feared danger.  \n", R.drawable.number2));
        persons.add(new Person("What is anxiety", "Anxiety is when you’re scared, worried, frightened, or nervous about something.\n\nAnxiety is normal. Everyone feels anxious or worried at times.  Sometimes we are extra worried or anxious when we think something bad is going to happen.  \n", R.drawable.number3));
        persons.add(new Person("Anxiety and ASD", "Young people with ASD are more likely to experience anxiety than people without ASD.\n\nAnxiety may be more common in people with ASD as people with autism may find it more challenging to cope with uncertainty\n", R.drawable.number4));
        persons.add(new Person("Anxiety Triggers", "An anxiety trigger is what leads you to feel anxious. Different things make different people anxious. \n\n" +
                "Triggers can be events, objects, thoughts or feelings.\n", R.drawable.number5));
        persons.add(new Person("Models of Anxiety", "The ABC model is used to explain the link between what has happened to you, how you think or feel about what has happened to you and your response.\n\n" +
                "In the ABC model:\n\n" +
                "“A” stands for ACTIVATING EVENT OR SITUATION\n\n" +
                "“B” stands for BELIEF SYSTEM\n\n" +
                "“C” stands for the emotional or behavioural CONSEQUENCES\n", R.drawable.number6));
        persons.add(new Person("Identifying Anxiety", "Identifying what sets off your anxiety will help you to make sense of your anxiety. With a better understanding of what causes your anxiety you can help reduce or even prevent it. \n", R.drawable.number7));
        persons.add(new Person("Managing Anxiety", "It’s very natural to want to avoid your fears and worries.\n\nOver time, it’s good to try and slowly stop avoiding your fears and worries as they you will become less scared of them.\n", R.drawable.number8));
    }
}